import { format, isValid } from 'date-fns';

export default function formatDateTime(dateTime) {
  if (dateTime && dateTime.seconds) {
    // Must convert from UNIX timestamp by multiplying for 1000
    return format(new Date(dateTime.seconds * 1000), 'DD/MM/YYYY HH:mm');
  }
  return null;
}

export function formatUnixTime(dateObject) {
  if (isValid(dateObject)) {
    return Math.floor(dateObject.getTime() / 1000);
  }
  return null;
}
