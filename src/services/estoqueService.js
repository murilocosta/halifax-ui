import connection from './connection';

class EstoqueService {
  constructor(connection) {
    this.connection = connection;
  }

  save(estoque) {
    if (estoque.id) {
      return this.connection.post('/estoque/estoque/alterar', estoque);
    }
    return this.connection.post('/estoque/estoque/criar', estoque);
  }

  findAll() {
    return this.connection.get('/estoque/estoque/listar');
  }
}

export default new EstoqueService(connection);
