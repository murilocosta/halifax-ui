import connection from './connection';

class ProdutoService {
  constructor(connection) {
    this.connection = connection;
  }

  save(produto) {
    if (produto.id) {
      return this.connection.post('/estoque/produto/alterar', produto);
    }
    return this.connection.post('/estoque/produto/criar', produto);
  }

  findAll() {
    return this.connection.get('/estoque/produto/listar');
  }

  addProdutoEstoque(estoqueId, produtoId, qtd) {
    const payload = { estoqueId, produtoId, qtd };
    return this.connection.post('/estoque/produto-estoque/adicionar', payload);
  }

  removeProdutoEstoque(estoqueId, produtoId, qtd) {
    const payload = { estoqueId, produtoId, qtd };
    return this.connection.post('/estoque/produto-estoque/remover', payload);
  }

  findByEstoque(estoqueId) {
    return this.connection.get(`/estoque/produto-estoque/listar?estoqueId=${estoqueId}`)
  }
}

export default new ProdutoService(connection);
