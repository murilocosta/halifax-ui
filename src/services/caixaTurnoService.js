import connection from './connection';

class CaixaTurnoService {
  constructor(connection) {
    this.connection = connection;
  }

  open(caixaTurno) {
    return this.connection.post('/caixa/caixa-turno/iniciar', caixaTurno);
  }

  close(caixaTurnoId) {
    return this.connection.post('/caixa/caixa-turno/finalizar', { turnoId: caixaTurnoId });
  }

  findAll(caixaId) {
    return this.connection.get(`/caixa/caixa-turno/listar-por-caixa?caixaId=${caixaId}`);
  }
}

export default new CaixaTurnoService(connection);
