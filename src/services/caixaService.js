import connection from './connection';

class CaixaService {
  constructor(connection) {
    this.connection = connection;
  }

  save(caixa) {
    if (caixa.id) {
      return this.connection.post('/caixa/caixa/atualizar', caixa);
    }
    return this.connection.post('/caixa/caixa/criar', caixa);
  }

  findAll() {
    return this.connection.get('/caixa/caixa/listar');
  }
}

export default new CaixaService(connection);
