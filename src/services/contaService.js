import connection from './connection';

class ContaService {
  constructor(connection) {
    this.connection = connection;
  }

  save(conta) {
    if (conta.id) {
      return this.connection.post('/financeiro/conta/alterar', conta);
    }
    return this.connection.post('/financeiro/conta/criar', conta);
  }

  findAll() {
    return this.connection.get('/financeiro/conta/listar');
  }
}

export default new ContaService(connection);
