import axios from 'axios';

const connection = axios.create({ 
    baseURL: 'http://192.168.25.18:8080'
});

export default connection;