import connection from './connection';

class CaixaRegistroService {
  constructor(connection) {
    this.connection = connection;
  }

  create(registro) {
    return this.connection.post('/caixa/registro/criar', registro);
  }

  update(registroId, descricao) {
    return this.connection.post('/caixa/registro/atualizar', {
      id: registroId, 
      descricao
    });
  }

  findAll(turnoId) {
    return this.connection.get(`/caixa/registro/listar-turno?turnoId=${turnoId}`);
  }
}

export default new CaixaRegistroService(connection);
