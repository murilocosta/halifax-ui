import connection from './connection';

class TipoLancamentoService {
  constructor(connection) {
    this.connection = connection;
  }

  save(tipoLancamento) {
    if (tipoLancamento.id) {
      return this.connection.post('/financeiro/tipoLancamento/alterar', tipoLancamento);
    }
    return this.connection.post('/financeiro/tipoLancamento/criar', tipoLancamento);
  }

  findAll() {
    return this.connection.get('/financeiro/tipoLancamento/listar');
  }
}

export default new TipoLancamentoService(connection);
