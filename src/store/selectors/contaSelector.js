import values from "lodash/values";

export const contaListSelector = ({contaState}) => ({contas: values(contaState.contas)});

export const contaSelector = ({contaState}) => ({conta: contaState.conta});
