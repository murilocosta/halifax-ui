import property from 'lodash/property';
import values from 'lodash/values';
import { createSelector } from 'reselect';

export const produtoEstoqueSelector = ({ produtoEstoqueState }) => ({ 
  produtosEstoque: values(produtoEstoqueState.produtosEstoque) 
});

export const produtoEstoqueFormSelector = createSelector(
  ({ estoqueState }) => estoqueState.estoqueOperacao,
  ({ produtoState }) => values(produtoState.produtos),
  (estoque, produtos) => ({ 
    estoqueId: property('id')(estoque), 
    produtos
  })
);
