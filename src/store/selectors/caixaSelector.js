import values from "lodash/values";

export const caixaSelector = ({ caixaState }) => ({
  caixa: caixaState.caixa
});

export const caixaListSelector = ({ caixaState }) => ({
  caixas: values(caixaState.caixas),
  caixa: caixaState.caixa
});

export const caixaTurnoSelector = ({ caixaState }) => ({
  caixa: caixaState.caixaTurno
});

export const caixaTurnoListSelector = ({ caixaState }) => ({
  caixas: values(caixaState.caixas),
  caixa: caixaState.caixaTurno
});
