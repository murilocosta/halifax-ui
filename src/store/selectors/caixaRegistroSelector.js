import values from 'lodash/values';

export const caixaRegistrosSelector = ({ caixaRegistroState }) => ({
  registros: values(caixaRegistroState.registros)
});
