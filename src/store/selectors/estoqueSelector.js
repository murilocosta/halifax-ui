import values from 'lodash/values';

export const estoqueListSelector = ({ estoqueState }) => ({ 
  estoques: values(estoqueState.estoques) 
});

export const estoqueSelector = ({ estoqueState }) => ({ 
  estoque: estoqueState.estoque 
});

export const estoqueOperacaoSelector = ({ estoqueState }) => ({
  estoque: estoqueState.estoqueOperacao
});

export const estoqueOperacaoListSelector = ({ estoqueState }) => ({
  estoques: values(estoqueState.estoques),
  estoque: estoqueState.estoqueOperacao
});
