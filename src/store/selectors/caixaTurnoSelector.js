import { createSelector } from 'reselect';
import values from 'lodash/values';
import find from 'lodash/find';

const turnosCaixaSelector = ({ caixaTurnoState }) => values(caixaTurnoState.turnos);

const hasTurnoAbertoSelector = createSelector(
  turnosCaixaSelector,
  (turnos) => find(turnos, (t) => t.fechamento == null)
);

export const turnoCaixaAbertoSelector = createSelector(
  ({ caixaRegistroState }) => values(caixaRegistroState.registros),
  hasTurnoAbertoSelector,
  (registros, turnoAberto) => ({ registros, turnoAberto })
);

export const turnoCaixaListSelector = createSelector(
  turnosCaixaSelector,
  hasTurnoAbertoSelector,
  (turnos, turnoAberto) => ({ turnos, turnoAberto })
);
