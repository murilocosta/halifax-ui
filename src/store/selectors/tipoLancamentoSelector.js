import values from "lodash/values";

export const tipoLancamentoListSelector = ({tipoLancamentoState}) => ({tipoLancamentos: values(tipoLancamentoState.tipoLancamentos)});

export const tipoLancamentoSelector = ({tipoLancamentoState}) => ({tipoLancamento: tipoLancamentoState.tipoLancamento});
