import values from "lodash/values";

export const produtoListSelector = ({ produtoState }) => ({ produtos: values(produtoState.produtos) });

export const produtoSelector = ({ produtoState }) => ({ produto: produtoState.produto });
