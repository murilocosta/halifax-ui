import keyBy from 'lodash/keyBy';

import {
  LIST_CONTA,
  PERSIST_CONTA,
  SELECT_CONTA,
  UNSELECT_CONTA,
  SELECT_CONTA_OPERACAO,
  UNSELECT_CONTA_OPERACAO
} from '../actions/contaActions';

const initialState = {
  contas: {},
  conta: null,
  contaOperacao: null
};

export default function contaReducer(state = initialState, action) {
  switch (action.type) {
    case PERSIST_CONTA:
      return {
        ...state,
        contas: {
          ...state.contas,
          [action.payload.id]: action.payload
        }
      };

    case SELECT_CONTA:
      return {
        ...state,
        conta: state.contas[action.payload]
      };

    case UNSELECT_CONTA:
      return {
        ...state,
        conta: null
      };

    case LIST_CONTA:
      return {
        ...state,
        contas: keyBy(action.payload, 'id')
      };

    case SELECT_CONTA_OPERACAO:
      return {
        ...state,
        contaOperacao: state.contas[action.payload]
      };

    case UNSELECT_CONTA_OPERACAO:
      return {
        ...state,
        contaOperacao: null
      };

    default:
      return state;
  }
}
