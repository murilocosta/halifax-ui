import keyBy from 'lodash/keyBy';

import {
  LIST_ESTOQUE,
  PERSIST_ESTOQUE,
  SELECT_ESTOQUE,
  UNSELECT_ESTOQUE,
  SELECT_ESTOQUE_OPERACAO,
  UNSELECT_ESTOQUE_OPERACAO
} from '../actions/estoqueActions';

const initialState = {
  estoques: {},
  estoque: null,
  estoqueOperacao: null
};

export default function estoqueReducer(state = initialState, action) {
  switch (action.type) {
    case PERSIST_ESTOQUE:
      return {
        ...state,
        estoques: {
          ...state.estoques,
          [action.payload.id]: action.payload
        }
      };

    case SELECT_ESTOQUE:
      return {
        ...state,
        estoque: state.estoques[action.payload]
      };

    case UNSELECT_ESTOQUE:
      return {
        ...state,
        estoque: null
      };

    case LIST_ESTOQUE:
      return {
        ...state,
        estoques: keyBy(action.payload, 'id')
      };

    case SELECT_ESTOQUE_OPERACAO:
      return {
        ...state,
        estoqueOperacao: state.estoques[action.payload]
      }

    case UNSELECT_ESTOQUE_OPERACAO:
      return {
        ...state,
        estoqueOperacao: null
      }

    default:
      return state;
  }
}
