import keyBy from 'lodash/keyBy';

import {
  LIST_CAIXA,
  PERSIST_CAIXA,
  SELECT_CAIXA,
  UNSELECT_CAIXA,
  SELECT_CAIXA_TURNO,
  UNSELECT_CAIXA_TURNO
} from '../actions/caixaActions';

const initialState = {
  caixas: {},
  caixa: null,
  caixaTurno: null
};

export default function caixaReducer(state = initialState, action) {
  switch (action.type) {
    case PERSIST_CAIXA:
      return {
        ...state,
        caixas: {
          ...state.caixas,
          [action.payload.id]: action.payload
        }
      };

    case LIST_CAIXA:
      return {
        ...state,
        caixas: keyBy(action.payload, 'id')
      };

    case SELECT_CAIXA:
      return {
        ...state,
        caixa: state.caixas[action.payload]
      };

    case UNSELECT_CAIXA:
      return {
        ...state,
        caixa: null
      };

    case SELECT_CAIXA_TURNO:
      return {
        ...state,
        caixaTurno: state.caixas[action.payload]
      };

    case UNSELECT_CAIXA_TURNO:
      return {
        ...state,
        caixaTurno: null
      };

    default:
      return state;
  }
}
