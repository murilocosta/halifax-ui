import keyBy from 'lodash/keyBy';

import {
  LIST_PRODUTO,
  PERSIST_PRODUTO,
  SELECT_PRODUTO,
  UNSELECT_PRODUTO
} from '../actions/produtoActions';

const initialState = {
  produtos: {},
  produto: null
};

export default function produtoReducer(state = initialState, action) {
  switch (action.type) {
    case PERSIST_PRODUTO:
      return {
        ...state,
        produtos: {
          ...state.produtos,
          [action.payload.id]: action.payload
        }
      };

    case SELECT_PRODUTO:
      return {
        ...state,
        produto: state.produtos[action.payload]
      };

    case UNSELECT_PRODUTO:
      return {
        ...state,
        produto: null
      };

    case LIST_PRODUTO:
      return {
        ...state,
        produtos: keyBy(action.payload, 'id')
      };

    default:
      return state;
  }
}
