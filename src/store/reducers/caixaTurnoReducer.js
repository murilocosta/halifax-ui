import keyBy from 'lodash/keyBy';

import {
  OPEN_TURNO_CAIXA,
  CLOSE_TURNO_CAIXA,
  LIST_TURNO_BY_CAIXA
} from '../actions/caixaTurnoActions';

const initalState = {
  turnos: {},
  turno: null
};

export default function caixaTurnoReducer(state = initalState, action) {
  switch (action.type) {
    case OPEN_TURNO_CAIXA:
      return {
        turnos: {
          ...state.turnos,
          [action.payload.turnoId]: action.payload
        }
      }

    case CLOSE_TURNO_CAIXA:
      const updatedTurno = {
        ...state.turnos[action.payload.turnoId],
        saldoFinal: action.payload.saldoFinal
      }

      return {
        turnos: {
          ...state.turnos,
          [action.payload.turnoId]: updatedTurno
        }
      }

    case LIST_TURNO_BY_CAIXA:
      return {
        turnos: keyBy(action.payload, 'id')
      };

    default:
      return state;
  }
}
