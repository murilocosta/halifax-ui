import keyBy from 'lodash/keyBy';

import { LIST_PRODUTO_ESTOQUE, ADD_PRODUTO_ESTOQUE, REMOVE_PRODUTO_ESTOQUE } from '../actions/produtoEstoqueActions';

const initialState = {
  produtosEstoque: {},
  produto: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LIST_PRODUTO_ESTOQUE:
      return {
        ...state,
        produtosEstoque: keyBy(action.payload, 'id')
      };

    case ADD_PRODUTO_ESTOQUE:
    case REMOVE_PRODUTO_ESTOQUE:
      const updatedProdutoEstoque = {
        ...state.produtosEstoque[action.payload.produtoId],
        qtd: action.payload.qtdAtual
      };
      return {
        ...state,
        produtosEstoque: {
          ...state.produtosEstoque,
          [action.payload.produtoId]: updatedProdutoEstoque
        }
      };

    default:
      return state;
  }
}
