import keyBy from 'lodash/keyBy';

import { LIST_TIPO_LANCAMENTO, PERSIST_TIPO_LANCAMENTO, SELECT_TIPO_LANCAMENTO, UNSELECT_TIPO_LANCAMENTO } from '../actions/tipoLancamentoActions';

const initialState = {
  tipoLancamentos: {},
  tipoLancamento: null
};

export default function tipoLancamentoReducer(state = initialState, action) {
  switch (action.type) {
    case PERSIST_TIPO_LANCAMENTO:
      return {
        ...state,
        tipoLancamentos: {
          ...state.tipoLancamentos,
          [action.payload.id]: action.payload
        }
      };

    case SELECT_TIPO_LANCAMENTO:
      return {
        ...state,
        tipoLancamento: state.tipoLancamentos[action.payload]
      };

    case UNSELECT_TIPO_LANCAMENTO:
      return {
        ...state,
        tipoLancamento: null
      };

    case LIST_TIPO_LANCAMENTO:
      return {
        ...state,
        tipoLancamentos: keyBy(action.payload, 'id')
      };

    default:
      return state;
  }
}
