import keyBy from 'lodash/keyBy';

import {
  CREATE_REGISTRO_CAIXA,
  UPDATE_REGISTRO_CAIXA,
  LIST_REGISTROS_CAIXA_TURNO
} from '../actions/caixaRegistroActions';

const initialState = {
  registros: {}
};

export default function CaixaRegistroReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_REGISTRO_CAIXA:
      return {
        registros: {
          ...state.registros,
          [action.payload.id]: {
            ...action.payload
          }
        }
      };

    case LIST_REGISTROS_CAIXA_TURNO:
      return {
        registros: keyBy(action.payload, 'id')
      };

    case UPDATE_REGISTRO_CAIXA:
    default:
      return state;
  }
}