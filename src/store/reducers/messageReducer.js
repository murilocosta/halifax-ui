import {ADD_MESSAGE, CLEAR_MESSAGES, REMOVE_MESSAGE} from "../actions/messageActions";

const initialState = {
    messages: []
};

export default function notificacaoReducer(state = initialState, action) {
    switch (action.type) {
        case ADD_MESSAGE:
            return {messages: state.messages.concat(action.payload)};

        case REMOVE_MESSAGE:
            return {messages: state.messages.filter((message) => message.content !== action.payload.content)};

        case CLEAR_MESSAGES:
            return initialState;

        default:
            return state;
    }
}
