import caixaTurnoService from '../../services/caixaTurnoService';
import { formatUnixTime } from '../../services/formatDateTime';

export const OPEN_TURNO_CAIXA = 'OPEN_TURNO_CAIXA';
export const CLOSE_TURNO_CAIXA = 'CLOSE_TURNO_CAIXA';
export const LIST_TURNO_BY_CAIXA = 'LIST_TURNO_BY_CAIXA';

export function openTurnoCaixa(caixaTurno) {
  return function (dispatch) {
    return caixaTurnoService.open(caixaTurno).then(function (response) {
      return dispatch({
        type: OPEN_TURNO_CAIXA,
        payload: {
          ...caixaTurno,
          id: response.data.turnoId,
          abertura: {
            seconds: formatUnixTime(new Date())
          }
        }
      });
    });
  };
}

export function closeTurnoCaixa(caixaTurnoId) {
  return function (dispatch) {
    return caixaTurnoService.close(caixaTurnoId).then(function (response) {
      return dispatch({
        type: CLOSE_TURNO_CAIXA,
        payload: response.data
      });
    });
  };
}

export function listTurnoByCaixa(caixaId) {
  return function (dispatch) {
    return caixaTurnoService.findAll(caixaId).then(function (response) {
      return dispatch({
        type: LIST_TURNO_BY_CAIXA,
        payload: response.data
      });
    });
  };
}
