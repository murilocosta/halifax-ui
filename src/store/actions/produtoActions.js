import produtoService from '../../services/produtoService';

export const PERSIST_PRODUTO = 'PERSIST_PRODUTO';
export const SELECT_PRODUTO = 'SELECT_PRODUTO';
export const UNSELECT_PRODUTO = 'UNSELECT_PRODUTO';
export const LIST_PRODUTO = 'LIST_PRODUTO';

export function persistProduto(produto) {
  return (dispatch) =>
    produtoService.save(produto).then(response =>
      dispatch({
        type: PERSIST_PRODUTO,
        payload: {
          ...produto,
          ...response.data
        }
      })
    );
}

export function selectProduto(produtoId) {
  return {
    type: SELECT_PRODUTO,
    payload: produtoId
  };
}

export function unselectProduto() {
  return {
    type: UNSELECT_PRODUTO
  };
}

export function listProduto() {
  return (dispatch) =>
    produtoService.findAll().then(response =>
      dispatch({
        type: LIST_PRODUTO,
        payload: response.data
      })
    );
}
