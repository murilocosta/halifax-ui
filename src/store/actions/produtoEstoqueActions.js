import property from 'lodash/property';

import produtoService from '../../services/produtoService';

export const ADD_PRODUTO_ESTOQUE = 'ADD_PRODUTO_ESTOQUE';
export const REMOVE_PRODUTO_ESTOQUE = 'REMOVE_PRODUTO_ESTOQUE';
export const LIST_PRODUTO_ESTOQUE = 'LIST_PRODUTO_ESTOQUE';

export function addProdutoEstoque(estoqueId, produtoId, qtd) {
  return (dispatch) =>
    produtoService.addProdutoEstoque(estoqueId, produtoId, qtd).then(response => 
      dispatch({
        type: ADD_PRODUTO_ESTOQUE,
        payload: {
          produtoId,
          ...response.data
        }
      })
    );
}

export function removeProdutoEstoque(estoqueId, produtoId, qtd) {
  return (dispatch) =>
    produtoService.removeProdutoEstoque(estoqueId, produtoId, qtd).then(response => 
      dispatch({
        type: REMOVE_PRODUTO_ESTOQUE,
        payload: {
          produtoId,
          ...response.data
        }
      })
    );
}

export function listProdutoEstoque(estoqueId) {
  return (dispatch) =>
    produtoService.findByEstoque(estoqueId).then(response =>
      dispatch({
        type: LIST_PRODUTO_ESTOQUE,
        payload: property('produtos')(response.data) || []
      })
    );
}
