import caixaService from '../../services/caixaService';

export const PERSIST_CAIXA = 'PERSIST_CAIXA';
export const LIST_CAIXA = 'LIST_CAIXA';

export const SELECT_CAIXA = 'SELECT_CAIXA';
export const UNSELECT_CAIXA = 'UNSELECT_CAIXA';
export const SELECT_CAIXA_TURNO = 'SELECT_CAIXA_TURNO';
export const UNSELECT_CAIXA_TURNO = 'UNSELECT_CAIXA_TURNO';

export function persistCaixa(caixa) {
  return (dispatch) =>
    caixaService.save(caixa).then(response =>
      dispatch({
        type: PERSIST_CAIXA,
        payload: {
          ...caixa,
          ...response.data
        }
      })
    );
}

export function listCaixa() {
  return (dispatch) =>
    caixaService.findAll().then(response =>
      dispatch({
        type: LIST_CAIXA,
        payload: response.data
      })
    );
}

export function selectCaixa(caixaId) {
  return {
    type: SELECT_CAIXA,
    payload: caixaId
  };
}

export function unselectCaixa() {
  return {
    type: UNSELECT_CAIXA
  };
}

export function selectCaixaTurno(caixaId) {
  return {
    type: SELECT_CAIXA_TURNO,
    payload: caixaId
  }
}

export function unselectCaixaTurno() {
  return {
    type: UNSELECT_CAIXA_TURNO
  }
}
