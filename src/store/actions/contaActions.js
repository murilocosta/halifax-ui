import contaService from '../../services/contaService';

export const PERSIST_CONTA = 'PERSIST_CONTA';
export const SELECT_CONTA = 'SELECT_CONTA';
export const UNSELECT_CONTA = 'UNSELECT_CONTA';
export const LIST_CONTA = 'LIST_CONTA';

export const SELECT_CONTA_OPERACAO = 'SELECT_CONTA_OPERACAO';
export const UNSELECT_CONTA_OPERACAO = 'UNSELECT_CONTA_OPERACAO';

export function persistConta(conta) {
  return (dispatch) =>
    contaService.save(conta).then(response =>
      dispatch({
        type: PERSIST_CONTA,
        payload: {
          ...conta,
          ...response.data
        }
      })
    );
}

export function selectConta(contaId) {
  return {
    type: SELECT_CONTA,
    payload: contaId
  };
}

export function unselectConta() {
  return {
    type: UNSELECT_CONTA
  };
}

export function listConta() {
  return (dispatch) =>
    contaService.findAll().then(response =>
      dispatch({
        type: LIST_CONTA,
        payload: response.data
      })
    );
}
