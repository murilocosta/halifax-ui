import tipoLancamentoService from '../../services/tipoLancamentoService';

export const PERSIST_TIPO_LANCAMENTO = 'PERSIST_TIPO_LANCAMENTO';
export const SELECT_TIPO_LANCAMENTO = 'SELECT_TIPO_LANCAMENTO';
export const UNSELECT_TIPO_LANCAMENTO = 'UNSELECT_TIPO_LANCAMENTO';
export const LIST_TIPO_LANCAMENTO = 'LIST_TIPO_LANCAMENTO';

export function persistTipoLancamento(tipoLancamento) {
  return (dispatch) =>
    tipoLancamentoService.save(tipoLancamento).then(response =>
      dispatch({
        type: PERSIST_TIPO_LANCAMENTO,
        payload: {
          ...tipoLancamento,
          ...response.data
        }
      })
    );
}

export function selectTipoLancamento(tipoLancamentoId) {
  return {
    type: SELECT_TIPO_LANCAMENTO,
    payload: tipoLancamentoId
  };
}

export function unselectTipoLancamento() {
  return {
    type: UNSELECT_TIPO_LANCAMENTO
  };
}

export function listTipoLancamento() {
  return (dispatch) =>
    tipoLancamentoService.findAll().then(response =>
      dispatch({
        type: LIST_TIPO_LANCAMENTO,
        payload: response.data
      })
    );
}
