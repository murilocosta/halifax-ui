export const ADD_MESSAGE = 'ADD_MESSAGE';
export const REMOVE_MESSAGE = 'REMOVE_MESSAGE';
export const CLEAR_MESSAGES = 'CLEAR_MESSAGES';

export function addMessage(message) {
    return {
        type: ADD_MESSAGE,
        payload: message,
    };
}

export function removeMessage(message) {
    return {
        type: REMOVE_MESSAGE,
        payload: message,
    };
}

export function clearMessages() {
    return {
        type: CLEAR_MESSAGES
    };
}
