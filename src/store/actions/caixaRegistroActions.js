import caixaRegistroService from '../../services/caixaRegistroService';

export const CREATE_REGISTRO_CAIXA = 'CREATE_REGISTRO_CAIXA';
export const UPDATE_REGISTRO_CAIXA = 'UPDATE_REGISTRO_CAIXA';
export const LIST_REGISTROS_CAIXA_TURNO = 'LIST_REGISTROS_CAIXA_TURNO';

export function createRegistroCaixa(registro) {
  return (dispatch) =>
    caixaRegistroService.create(registro).then((response) =>
      dispatch({
        type: CREATE_REGISTRO_CAIXA,
        payload: { ...registro, ...response.data }
      })
    );
}

export function updateRegistroCaixa(registroId, descricao) {
  return (dispatch) =>
    caixaRegistroService.update(registroId, descricao).then((response) =>
      dispatch({
        type: UPDATE_REGISTRO_CAIXA,
        payload: response.data
      })
    );
}

export function listRegistrosCaixaTurno(turnoId) {
  return (dispatch) =>
    caixaRegistroService.findAll(turnoId).then((response) =>
      dispatch({
        type: LIST_REGISTROS_CAIXA_TURNO,
        payload: response.data
      })
    );
}
