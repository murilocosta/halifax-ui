import estoqueService from '../../services/estoqueService';

export const PERSIST_ESTOQUE = 'PERSIST_ESTOQUE';
export const SELECT_ESTOQUE = 'SELECT_ESTOQUE';
export const UNSELECT_ESTOQUE = 'UNSELECT_ESTOQUE';
export const LIST_ESTOQUE = 'LIST_ESTOQUE';

export const SELECT_ESTOQUE_OPERACAO = 'SELECT_ESTOQUE_OPERACAO';
export const UNSELECT_ESTOQUE_OPERACAO = 'UNSELECT_ESTOQUE_OPERACAO';

export function persistEstoque(estoque) {
  return (dispatch) =>
    estoqueService.save(estoque).then(response =>
      dispatch({
        type: PERSIST_ESTOQUE,
        payload: {
          ...estoque,
          ...response.data
        }
      })
    );
}

export function selectEstoque(estoqueId) {
  return {
    type: SELECT_ESTOQUE,
    payload: estoqueId
  };
}

export function unselectEstoque() {
  return {
    type: UNSELECT_ESTOQUE
  };
}

export function listEstoque() {
  return (dispatch) =>
    estoqueService.findAll().then(response =>
      dispatch({
        type: LIST_ESTOQUE,
        payload: response.data
      })
    );
}

export function selectEstoqueOperacao(estoqueId) {
  return {
    type: SELECT_ESTOQUE_OPERACAO,
    payload: estoqueId
  };
}

export function unselectEstoqueOperacao() {
  return {
    type: UNSELECT_ESTOQUE_OPERACAO
  };
}
