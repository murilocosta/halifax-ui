import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import estoqueReducer from './reducers/estoqueReducer';
import produtoReducer from './reducers/produtoReducer';
import caixaReducer from './reducers/caixaReducer';
import contaReducer from './reducers/contaReducer';
import tipoLancamentoReducer from './reducers/tipoLancamentoReducer';
import messageReducer from './reducers/messageReducer';
import produtoEstoqueReducer from './reducers/produtoEstoqueReducer';
import caixaTurnoReducer from './reducers/caixaTurnoReducer';
import caixaRegistroReducer from './reducers/caixaRegistroReducer';

const rootReducer = combineReducers({
    estoqueState: estoqueReducer,
    produtoState: produtoReducer,
    produtoEstoqueState: produtoEstoqueReducer,
    caixaState: caixaReducer,
    caixaTurnoState: caixaTurnoReducer,
    caixaRegistroState: caixaRegistroReducer,
    contaState: contaReducer,
    tipoLancamentoState: tipoLancamentoReducer,
    messageState: messageReducer,
});

const persistedState = localStorage.getItem('applicationState')
    ? JSON.parse(localStorage.getItem('applicationState'))
    : {};

const store = createStore(
    rootReducer,
    persistedState,
    applyMiddleware(thunk)
);

store.subscribe(function () {
    localStorage.setItem('applicationState', JSON.stringify(store.getState()));
});

export default store;
