import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Container from 'react-bootstrap/es/Container';

import AppHeader from './AppHeader';
import AppNavigation from './AppNavigation';
import AppMessages from './AppMessages';

export default function Home() {
  return (
    <Router>
      <Container>
        <AppHeader />
        <AppMessages />
        <AppNavigation />
      </Container>
    </Router>
  );
}
