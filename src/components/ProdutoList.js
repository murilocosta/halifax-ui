import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/es/Table';

import { produtoListSelector } from '../store/selectors/produtoSelector';
import { listProduto } from '../store/actions/produtoActions';
import ProdutoListItem from './ProdutoListItem';

const tableActionsStyle = { textAlign: 'right' };

class ProdutoList extends PureComponent {
  componentDidMount() {
    this.props.listProduto();
  }

  render() {
    return (
      <Table responsive>
        <thead>
        <tr>
          <th>#</th>
          <th>Cód.</th>
          <th>Descrição</th>
          <th style={tableActionsStyle}>Ações</th>
        </tr>
        </thead>
        <tbody>
        {(this.props.produtos || []).map((p) => <ProdutoListItem key={p.id} produto={p} />)}
        </tbody>
      </Table>
    );
  }
}

export default connect(produtoListSelector, { listProduto })(ProdutoList);
