import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/es/Table';

import { estoqueListSelector } from '../store/selectors/estoqueSelector';
import { listEstoque } from '../store/actions/estoqueActions';
import EstoqueListItem from './EstoqueListItem';

const tableActionsStyle = { textAlign: 'right' };

class EstoqueList extends PureComponent {
  componentDidMount() {
    this.props.listEstoque();
  }

  render() {
    return (
      <Table responsive>
        <thead>
        <tr>
          <th>#</th>
          <th>Nome</th>
          <th style={tableActionsStyle}>Ações</th>
        </tr>
        </thead>
        <tbody>
        {(this.props.estoques || []).map((e) => <EstoqueListItem key={e.id} estoque={e} />)}
        </tbody>
      </Table>
    );
  }
}

export default connect(estoqueListSelector, { listEstoque })(EstoqueList);
