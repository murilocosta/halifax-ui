import React from 'react';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/es/Table';

import { produtoEstoqueSelector } from '../store/selectors/produtoEstoqueSelector'

function EstoqueOperacaoTableRow({ produtoEstoque }) {
  return (
    <tr>
      <td>{produtoEstoque.id}</td>
      <td>{`${produtoEstoque.descricao} (${produtoEstoque.cod})`}</td>
      <td>{produtoEstoque.qtd}</td>
    </tr>
  );
}

function EstoqueOperacaoTable({ produtosEstoque }) {
  return (
    <Table responsive>
      <thead>
        <tr>
          <th>#</th>
          <th>Produto</th>
          <th>Qtd.</th>
        </tr>
      </thead>
      <tbody>
        {produtosEstoque.map((p, index) => (
          <EstoqueOperacaoTableRow key={index} produtoEstoque={p} />
        ))}
      </tbody>
    </Table>
  );
}

export default connect(produtoEstoqueSelector, {})(EstoqueOperacaoTable);
