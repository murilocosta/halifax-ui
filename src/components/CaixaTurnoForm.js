import React from 'react';
import { connect } from 'react-redux';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { Button, ButtonGroup, Form as BootForm } from 'react-bootstrap';

import { caixaTurnoSelector } from '../store/selectors/caixaSelector';
import { openTurnoCaixa, listTurnoByCaixa } from '../store/actions/caixaTurnoActions';
import CaixaTurnoSchema from '../validators/CaixaTurnoSchema';
import AppFormControl from './AppFormControl';

const initialValues = {
  fundoCaixa: 0.00,
  caixaId: 0
}

const CaixaTurnoForm = ({ caixa, openTurnoCaixa, listTurnoByCaixa }) => {
  function _handleSubmit(values, { resetForm, setSubmitting }) {
    openTurnoCaixa(values).then(function () {
      setSubmitting(false);
      resetForm();
      listTurnoByCaixa(values.caixaId);
    }).catch(function () {
      setSubmitting(false);
    });
  }

  return (
    <React.Fragment>
      <Formik
        enableReinitialize
        initialValues={{ ...initialValues, caixaId: caixa.id }}
        validationSchema={CaixaTurnoSchema}
        onSubmit={_handleSubmit}
      >
        {({ resetForm, isSubmitting }) => (
          <Form>
            <Field type="hidden" name="caixaId" component={AppFormControl} />

            <BootForm.Group controlId="fundoCaixa">
              <BootForm.Label>Fundo de caixa</BootForm.Label>
              <Field type="number" name="fundoCaixa" component={AppFormControl} />
              <ErrorMessage name="fundoCaixa" component={BootForm.Text} />
            </BootForm.Group>

            <ButtonGroup>
              <Button type="submit" disabled={isSubmitting}>
                {'Abrir turno'}
              </Button>
            </ButtonGroup>
          </Form>
        )}
      </Formik>
      <br />
    </React.Fragment>
  );
};

export default connect(caixaTurnoSelector, {
  openTurnoCaixa,
  listTurnoByCaixa
})(
  CaixaTurnoForm
);
