import React, { useState } from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faEye } from '@fortawesome/free-solid-svg-icons';

import formatDateTime from '../services/formatDateTime';
import AppModal from './AppModal';
import CaixaRegistroTable from './CaixaRegistroTable';

export default function CaixaTurnoTableRow({ turno, onCloseTurno }) {
  const [show, setShow] = useState(false);

  return (
    <tr>
      <td>{turno.id}</td>
      <td>{formatDateTime(turno.abertura)}</td>
      <td>{formatDateTime(turno.fechamento)}</td>
      <td>{turno.fundoCaixa}</td>
      <td>{turno.saldoFinal}</td>
      <td>
        <ButtonGroup>
          {turno.fechamento ? (
            <Button title={'Ver registros'} onClick={() => setShow(true)}>
              <FontAwesomeIcon icon={faEye} />
            </Button>
          ) : (
            <Button title={'Fechar turno'} onClick={() => onCloseTurno(turno)}>
              <FontAwesomeIcon icon={faTimes} />
            </Button>
          )}
        </ButtonGroup>

        <AppModal title={'Lista de registros'} show={show} onClose={() => setShow(false)}>
          <CaixaRegistroTable turnoId={turno.id} />
        </AppModal>
      </td>
    </tr>
  );
}