import React from 'react';

import ProdutoForm from './ProdutoForm';
import ProdutoList from './ProdutoList';

function ProdutoManager() {
  return (
    <section>
      <h2 className="page-header">Gerenciar produtos</h2>
      <ProdutoForm />
      <br />
      <ProdutoList />
    </section>
  );
}

export default ProdutoManager;
