import React from 'react';
import { connect } from 'react-redux';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import Button from 'react-bootstrap/es/Button';
import BootForm from 'react-bootstrap/es/Form';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';

import { caixaSelector } from '../store/selectors/caixaSelector';
import { persistCaixa, unselectCaixa } from '../store/actions/caixaActions';
import CaixaSchema from '../validators/CaixaSchema';
import AppFormControl from './AppFormControl';

const initialValues = {
  nome: ''
};

function CaixaForm({ caixa, persistCaixa, unselectCaixa }) {
  function _handleSubmit(values, { resetForm, setSubmitting }) {
    persistCaixa(values).then(function () {
      setSubmitting(false);
      resetForm();
      unselectCaixa();
    });
  }

  return (
    <Formik
      enableReinitialize
      initialValues={caixa || initialValues}
      validationSchema={CaixaSchema}
      onSubmit={_handleSubmit}
    >
      {({ isSubmitting, resetForm }) => (
        <Form>
          <BootForm.Group controlId="formCaixaNome">
            <BootForm.Label>Nome</BootForm.Label>
            <Field type="text" name="nome" component={AppFormControl} />
            <ErrorMessage name="nome" component={BootForm.Text} />
          </BootForm.Group>

          <ButtonGroup>
            <Button variant="danger" onClick={() => {
              unselectCaixa();
              resetForm();
            }}>
              {'Cancelar'}
            </Button>

            <Button type="submit" disabled={isSubmitting}>
              {'Enviar'}
            </Button>
          </ButtonGroup>
        </Form>
      )}
    </Formik>
  );
}

export default connect(caixaSelector, { persistCaixa, unselectCaixa })(CaixaForm);
