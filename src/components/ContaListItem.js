import React from 'react';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';
import Button from 'react-bootstrap/es/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';

const tableActionsStyle = { textAlign: 'right' };

export default function ContaListItem({ conta, onSelectConta }) {
  return (
    <tr>
      <td>{conta.id}</td>
      <td>{conta.nome}</td>
      <td style={tableActionsStyle}>
        <ButtonGroup>
          <Button title={'Atualizar'} onClick={() => onSelectConta(conta.id)}>
            <FontAwesomeIcon icon={faPen} />
          </Button>
        </ButtonGroup>
      </td>
    </tr>
  );
}
