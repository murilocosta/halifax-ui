import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-bootstrap';

import { turnoCaixaAbertoSelector } from '../store/selectors/caixaTurnoSelector';
import { listRegistrosCaixaTurno } from '../store/actions/caixaRegistroActions';
import CaixaRegistroForm from './CaixaRegistroForm';
import CaixaAcompanhamentoTable from './CaixaAcompanhamentoTable'

function CaixaRegistroManager({ registros, turnoAberto, listRegistrosCaixaTurno }) {
  useEffect(function () {
    if (turnoAberto) {
      listRegistrosCaixaTurno(turnoAberto.id);
    }
  }, [
    turnoAberto,
    listRegistrosCaixaTurno
  ]);

  if (!turnoAberto) {
    return (
      <Alert variant={'warning'}>
        {'Não há turno aberto para esse caixa'}
      </Alert>
    );
  }

  return (
    <React.Fragment>
      <CaixaRegistroForm caixaTurnoId={turnoAberto.id} />
      <br />
      <CaixaAcompanhamentoTable registros={registros} />
    </React.Fragment>
  );
}

export default connect(turnoCaixaAbertoSelector, { listRegistrosCaixaTurno })(CaixaRegistroManager);
