import React from 'react';
import Form from 'react-bootstrap/es/Form';

export default function AppFromControl({ field, form: { touched, errors }, ...props }) {
  return (
    <Form.Control
      isInvalid={touched[field.name] && errors[field.name]}
      {...field}
      {...props}
    />
  );
}
