import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import Button from 'react-bootstrap/es/Button';
import BootForm from 'react-bootstrap/es/Form';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';

import { contaSelector } from '../store/selectors/contaSelector';
import { persistConta, unselectConta } from '../store/actions/contaActions';
import ContaSchema from '../validators/ContaSchema';
import AppFormControl from './AppFormControl';

const initialValues = {
  nome: ''
};

class ContaForm extends PureComponent {

  handleSubmit = (values, { setSubmitting, resetForm }) => {
    this.props.persistConta(values).then(function () {
      resetForm();
      setSubmitting(false);
    });
    this.props.unselectConta();
  };

  render() {
    return (
      <Formik
        enableReinitialize
        initialValues={(this.props.conta || initialValues)}
        validationSchema={ContaSchema}
        onSubmit={this.handleSubmit}
      >
        {({ resetForm, isSubmitting }) => (
          <Form>
            <BootForm.Group controlId="formContaNome">
              <BootForm.Label>Nome</BootForm.Label>
              <Field type="text" name="nome" component={AppFormControl} />
              <ErrorMessage name="nome" component={BootForm.Text} />
            </BootForm.Group>

            <ButtonGroup>
              <Button variant="danger" onClick={() => {
                this.props.unselectConta();
                resetForm();
              }}>
                {'Cancelar'}
              </Button>

              <Button type="submit" disabled={isSubmitting}>
                {'Enviar'}
              </Button>
            </ButtonGroup>
          </Form>
        )}
      </Formik>
    );
  }

}

export default connect(contaSelector, { persistConta, unselectConta })(ContaForm);
