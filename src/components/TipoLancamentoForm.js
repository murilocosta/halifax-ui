import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import Button from 'react-bootstrap/es/Button';
import BootForm from 'react-bootstrap/es/Form';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';

import { tipoLancamentoSelector } from '../store/selectors/tipoLancamentoSelector';
import { persistTipoLancamento, unselectTipoLancamento } from '../store/actions/tipoLancamentoActions';
import TipoLancamentoSchema from '../validators/TipoLancamentoSchema';
import AppFormControl from './AppFormControl';

const initialValues = {
  nome: ''
};

class TipoLancamentoForm extends PureComponent {

  handleSubmit = (values, { setSubmitting, resetForm }) => {
    this.props.persistTipoLancamento(values).then(function () {
      resetForm();
      setSubmitting(false);
    });
    this.props.unselectTipoLancamento();
  };

  render() {
    return (
      <Formik
        enableReinitialize
        initialValues={(this.props.tipoLancamento || initialValues)}
        validationSchema={TipoLancamentoSchema}
        onSubmit={this.handleSubmit}
      >
        {({ resetForm, isSubmitting }) => (
          <Form>
            <BootForm.Group controlId="formTipoLancamentoNome">
              <BootForm.Label>Nome</BootForm.Label>
              <Field type="text" name="nome" component={AppFormControl} />
              <ErrorMessage name="nome" component={BootForm.Text} />
            </BootForm.Group>

            <ButtonGroup>
              <Button variant="danger" onClick={() => {
                this.props.unselectTipoLancamento();
                resetForm();
              }}>
                {'Cancelar'}
              </Button>

              <Button type="submit" disabled={isSubmitting}>
                {'Enviar'}
              </Button>
            </ButtonGroup>
          </Form>
        )}
      </Formik>
    );
  }

}

export default connect(tipoLancamentoSelector, { persistTipoLancamento, unselectTipoLancamento })(TipoLancamentoForm);
