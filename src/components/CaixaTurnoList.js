import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { ListGroup } from 'react-bootstrap';
import property from 'lodash/property';

import { caixaTurnoListSelector } from '../store/selectors/caixaSelector';
import { listCaixa, selectCaixaTurno } from '../store/actions/caixaActions';
import { listTurnoByCaixa } from '../store/actions/caixaTurnoActions';

function CaixaTurnoList({ caixa, caixas, listCaixa, listTurnoByCaixa, selectCaixaTurno }) {
  useEffect(() => {
    listCaixa();
  }, [listCaixa]);

  function _handleClick(caixaId) {
    selectCaixaTurno(caixaId);
    listTurnoByCaixa(caixaId);
  }

  return (
    <ListGroup variant="pills" className="flex-column" defaultActiveKey={property('id')(caixa)}>
      {caixas.map(c => (
        <ListGroup.Item action key={c.id} eventKey={c.id} onClick={() => _handleClick(c.id)}>
          {c.nome}
        </ListGroup.Item>
      ))}
    </ListGroup>
  );
}

export default connect(caixaTurnoListSelector, {
  listCaixa,
  listTurnoByCaixa,
  selectCaixaTurno
})(
  CaixaTurnoList
);
