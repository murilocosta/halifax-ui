import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/es/Table';

import { tipoLancamentoListSelector } from '../store/selectors/tipoLancamentoSelector';
import { listTipoLancamento } from '../store/actions/tipoLancamentoActions';
import TipoLancamentoListItem from './TipoLancamentoListItem';

const tableActionsStyle = { textAlign: 'right' };

class TipoLancamentoList extends PureComponent {
  componentDidMount() {
    this.props.listTipoLancamento();
  }

  render() {
    return (
      <Table responsive>
        <thead>
        <tr>
          <th>#</th>
          <th>Nome</th>
          <th style={tableActionsStyle}>Ações</th>
        </tr>
        </thead>
        <tbody>
        {(this.props.tipoLancamentos || []).map((e) => <TipoLancamentoListItem key={e.id} tipoLancamento={e} />)}
        </tbody>
      </Table>
    );
  }
}

export default connect(tipoLancamentoListSelector, { listTipoLancamento })(TipoLancamentoList);
