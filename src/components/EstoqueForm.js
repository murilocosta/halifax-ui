import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import Button from 'react-bootstrap/es/Button';
import BootForm from 'react-bootstrap/es/Form';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';

import { estoqueSelector } from '../store/selectors/estoqueSelector';
import { persistEstoque, unselectEstoque } from '../store/actions/estoqueActions';
import EstoqueSchema from '../validators/EstoqueSchema';
import AppFormControl from './AppFormControl';

const initialValues = {
  nome: ''
};

class EstoqueForm extends PureComponent {

  handleSubmit = (values, { setSubmitting, resetForm }) => {
    this.props.persistEstoque(values).then(function () {
      resetForm();
      setSubmitting(false);
    });
    this.props.unselectEstoque();
  };

  render() {
    return (
      <Formik
        enableReinitialize
        initialValues={(this.props.estoque || initialValues)}
        validationSchema={EstoqueSchema}
        onSubmit={this.handleSubmit}
      >
        {({ resetForm, isSubmitting }) => (
          <Form>
            <BootForm.Group controlId="formEstoqueNome">
              <BootForm.Label>Nome</BootForm.Label>
              <Field type="text" name="nome" component={AppFormControl} />
              <ErrorMessage name="nome" component={BootForm.Text} />
            </BootForm.Group>

            <ButtonGroup>
              <Button variant="danger" onClick={() => {
                this.props.unselectEstoque();
                resetForm();
              }}>
                {'Cancelar'}
              </Button>

              <Button type="submit" disabled={isSubmitting}>
                {'Enviar'}
              </Button>
            </ButtonGroup>
          </Form>
        )}
      </Formik>
    );
  }

}

export default connect(estoqueSelector, { persistEstoque, unselectEstoque })(EstoqueForm);
