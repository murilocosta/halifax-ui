import React from 'react';
import { connect } from 'react-redux';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';
import Button from 'react-bootstrap/es/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';

import { selectTipoLancamento } from '../store/actions/tipoLancamentoActions';

const tableActionsStyle = { textAlign: 'right' };

function TipoLancamentoListItem({ tipoLancamento, selectTipoLancamento }) {
  return (
    <tr>
      <td>{tipoLancamento.id}</td>
      <td>{tipoLancamento.nome}</td>
      <td style={tableActionsStyle}>
        <ButtonGroup>
          <Button title={'Atualizar'} onClick={() => selectTipoLancamento(tipoLancamento.id)}>
            <FontAwesomeIcon icon={faPen} />
          </Button>
        </ButtonGroup>
      </td>
    </tr>
  );
}

export default connect(null, { selectTipoLancamento })(TipoLancamentoListItem);