import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import Button from 'react-bootstrap/es/Button';
import BootForm from 'react-bootstrap/es/Form';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';

import { produtoSelector } from '../store/selectors/produtoSelector';
import { persistProduto, unselectProduto } from '../store/actions/produtoActions';
import ProdutoSchema from '../validators/ProdutoSchema';
import AppFormControl from './AppFormControl';

const initialValues = {
  cod: '',
  descricao: ''
};

class ProdutoForm extends PureComponent {

  handleSubmit = (values, { setSubmitting, resetForm }) => {
    this.props.persistProduto(values).then(function () {
      resetForm();
      setSubmitting(false);
    });
    this.props.unselectProduto();
  };

  render() {
    return (
      <Formik
        enableReinitialize
        initialValues={(this.props.produto || initialValues)}
        validationSchema={ProdutoSchema}
        onSubmit={this.handleSubmit}
      >
        {({ resetForm, isSubmitting }) => (
          <Form>
            <BootForm.Group controlId="formProdutoCod">
              <BootForm.Label>Código</BootForm.Label>
              <Field type="text" name="cod" component={AppFormControl} />
              <ErrorMessage name="cod" component={BootForm.Text} />
            </BootForm.Group>

            <BootForm.Group controlId="formProdutoDescricao">
              <BootForm.Label>Descrição</BootForm.Label>
              <Field type="text" name="descricao" component={AppFormControl} />
              <ErrorMessage name="descricao" component={BootForm.Text} />
            </BootForm.Group>

            <ButtonGroup>
              <Button variant="danger" onClick={() => {
                this.props.unselectProduto();
                resetForm();
              }}>
                {'Cancelar'}
              </Button>

              <Button type="submit" disabled={isSubmitting}>
                {'Enviar'}
              </Button>
            </ButtonGroup>
          </Form>
        )}
      </Formik>
    );
  }

}

export default connect(produtoSelector, { persistProduto, unselectProduto })(ProdutoForm);
