import React from 'react';
import { connect } from 'react-redux';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { Button, ButtonGroup, Form as BootForm } from 'react-bootstrap';
import DatePicker from 'react-datepicker';

import { formatUnixTime } from '../services/formatDateTime';
import { createRegistroCaixa } from '../store/actions/caixaRegistroActions';
import CaixaRegistroSchema from '../validators/CaixaRegistroSchema';
import AppFormControl from './AppFormControl';
import './CaixaRegistroForm.css';

const initialValues = {
  caixaTurnoId: null,
  valor: 0.00,
  descricao: '',
  emissao: new Date()
};

function CaixaRegistroForm({ caixaTurnoId, createRegistroCaixa }) {

  function _handleSubmit(values, { setSubmitting, resetForm }) {
    createRegistroCaixa({ ...values, emissao: { seconds: formatUnixTime(values.emissao) } })
      .then(function () {
        setSubmitting(false);
        resetForm();
      })
      .catch(function () {
        setSubmitting(false);
      });
  }

  return (
    <Formik
      enableReinitialize
      initialValues={{ ...initialValues, caixaTurnoId }}
      validationSchema={CaixaRegistroSchema}
      onSubmit={_handleSubmit}
    >
      {({ values, setFieldValue, isSubmitting }) => (
        <Form>
          <Field type="hidden" name="caixaTurnoId" component={AppFormControl} />

          <BootForm.Group controlId="formRegistroDescricao">
            <BootForm.Label>Descrição</BootForm.Label>
            <Field type="text" name="descricao" component={AppFormControl} />
            <ErrorMessage name="descricao" component={BootForm.Text} />
          </BootForm.Group>

          <BootForm.Group controlId="formRegistroValor">
            <BootForm.Label>Valor</BootForm.Label>
            <Field type="number" name="valor" component={AppFormControl} />
            <ErrorMessage name="valor" component={BootForm.Text} />
          </BootForm.Group>

          <BootForm.Group controlId="formRegistroEmissao">
            <BootForm.Label>Date de emissão</BootForm.Label>
            <br />
            <DatePicker
              showTimeSelect
              className="calendar-input"
              timeFormat="HH:mm"
              dateFormat="dd/MM/yyyy HH:mm"
              selected={values.emissao}
              onChange={(emissao) => setFieldValue('emissao', emissao)}
              customInput={<BootForm.Control />}
            />
            <ErrorMessage name="emissao" component={BootForm.Text} />
          </BootForm.Group>

          <ButtonGroup>
            <Button type="submit" disabled={isSubmitting}>
              {'Enviar'}
            </Button>
          </ButtonGroup>
        </Form>
      )}
    </Formik>
  );
}

export default connect(null, { createRegistroCaixa })(CaixaRegistroForm);