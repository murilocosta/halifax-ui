import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Table, Alert } from 'react-bootstrap';
import isEmpty from 'lodash/isEmpty';

import { listRegistrosCaixaTurno } from '../store/actions/caixaRegistroActions';
import { caixaRegistrosSelector } from '../store/selectors/caixaRegistroSelector';
import formatDateTime from '../services/formatDateTime';

const CaixaRegistroTable = ({ turnoId, registros = [], listRegistrosCaixaTurno }) => {
  useEffect(function () {
    listRegistrosCaixaTurno(turnoId);
  }, [
    turnoId, 
    listRegistrosCaixaTurno
  ]);

  if (isEmpty(registros)) {
    return (
      <Alert variant={'info'}>{'Não há registros para esse turno'}</Alert>
    );
  }

  return (
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Emissão</th>
          <th>Descrição</th>
          <th>Valor</th>
        </tr>
      </thead>
      <tbody>
        {registros.map((r, index) => (
          <tr key={index}>
            <td>{r.id}</td>
            <td>{formatDateTime(r.emissao)}</td>
            <td>{r.descricao}</td>
            <td>{r.valor}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default connect(caixaRegistrosSelector, { listRegistrosCaixaTurno })(CaixaRegistroTable);