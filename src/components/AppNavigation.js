import React from 'react';
import { Route, Switch } from 'react-router-dom';

import EstoqueManager from './EstoqueManager';
import ProdutoManager from './ProdutoManager';
import CaixaManager from './CaixaManager';
import ContaManager from './ContaManager';
import TipoLancamentoManager from './TipoLancamentoManager';
import EstoqueOperacao from './EstoqueOperacao';
import CaixaTurnoManager from './CaixaTurnoManager';
import CaixaAcompanhamento from './CaixaAcompanhamento';

const Empty = () => (<div />);

export default function MobeeNavigation() {
  return (
    <Switch>
      <Route exact path="/" component={Empty} />

      <Route path="/produtos" component={ProdutoManager} />
      <Route path="/contas" component={ContaManager} />
      <Route path="/tipos-lancamento" component={TipoLancamentoManager} />

      <Route exact path="/estoques" component={EstoqueManager} />
      <Route path="/estoques/operacoes" component={EstoqueOperacao} />
      
      <Route exact path="/caixas" component={CaixaManager} />
      <Route path="/caixas/turnos" component={CaixaTurnoManager} />
      <Route path="/caixas/acompanhamento" component={CaixaAcompanhamento} />

      <Route path="/financeiro/operacoes" component={Empty} />
      <Route path="/financeiro/agendamentos" component={Empty} />
      <Route path="/financeiro/contas" component={Empty} />
    </Switch>
  );
}
