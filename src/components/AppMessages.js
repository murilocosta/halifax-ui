import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Alert from 'react-bootstrap/es/Alert';

import { clearMessages, removeMessage } from '../store/actions/messageActions';
import { messagesSelector } from '../store/selectors/messageSelector';

const AppMessage = ({ message, removeMessage }) => {
  return (
    <Alert
      show={!!message}
      variant={message.type}
      onClose={() => removeMessage(message)}
    >
      {this.props.message.content}
    </Alert>
  );
};

const AppMessages = ({ messages, onRemoveMessage }) => {
  return messages.map((message, index) => (
    <AppMessage
      key={message.content}
      message={message}
      onRemoveMessage={onRemoveMessage}
    />
  ));
};

export default withRouter(connect(messagesSelector, { removeMessage, clearMessages })(AppMessages));
