import React from 'react';
import { Spinner } from 'react-bootstrap';

import './AppLoading.css'

export default function AppLoading() {
  return (
    <div className="app-loading">
      <Spinner animation="border" size="lg" />
    </div>
  );
}