import React from 'react';

import EstoqueForm from './EstoqueForm';
import EstoqueList from './EstoqueList';

function EstoqueManager() {
  return (
    <section>
      <h2 className="page-header">Gerenciar estoques</h2>
      <EstoqueForm />
      <br />
      <EstoqueList />
    </section>
  );
}

export default EstoqueManager;
