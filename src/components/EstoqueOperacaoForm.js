import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { Button, ButtonGroup, Form as BootForm } from 'react-bootstrap';
import { Typeahead } from 'react-bootstrap-typeahead';

import EstoqueOperacaoSchema from '../validators/EstoqueOperacaoSchema';
import { produtoEstoqueFormSelector } from '../store/selectors/produtoEstoqueSelector';
import { listProduto } from '../store/actions/produtoActions';
import { addProdutoEstoque, removeProdutoEstoque } from '../store/actions/produtoEstoqueActions';
import AppFormControl from './AppFormControl';

const initialValues = {
  estoqueId: 0,
  produto: [],
  qtd: ''
};

class EstoqueOperacaoForm extends PureComponent {

  componentDidMount() {
    this.props.listProduto();
  }

  handleSubmit = ({op, estoqueId, produto, qtd}, { setSubmitting, resetForm }) => {
    this.executeOperation(op, estoqueId, produto[0].id, qtd)
      .then(function () {
        setSubmitting(false);
        resetForm();
      })
      .catch(function () {
        setSubmitting(false);
      });
  };

  executeOperation(op, estoqueId, produtoId, qtd) {
    if (op === 'ADD') {
      return this.props.addProdutoEstoque(estoqueId, produtoId, qtd)
    } else if (op === 'REM') {
      return this.props.removeProdutoEstoque(estoqueId, produtoId, qtd)
    } else {
      return Promise.reject();
    }
  }

  render() {
    const formValues = { ...initialValues, estoqueId: this.props.estoqueId }
    return (
      <Formik
        enableReinitialize
        initialValues={formValues}
        validationSchema={EstoqueOperacaoSchema}
        onSubmit={this.handleSubmit}
      >
        {({ values, setFieldValue, resetForm, isSubmitting }) => (
          <Form>
            <Field type="hidden" name="estoqueId" component={AppFormControl} />

            <BootForm.Group controlId="formEstoqueOperacaoProduto">
              <BootForm.Label>Produto</BootForm.Label>
              <Typeahead
                id="produtoId"
                labelKey="descricao"
                options={this.props.produtos}
                onChange={(p) => setFieldValue('produto', p)}
                selected={values.produto}
              />
              <ErrorMessage name="produto" component={BootForm.Text} />
            </BootForm.Group>

            <BootForm.Group controlId="formEstoqueOperacaoQtd">
              <BootForm.Label>Qtd.</BootForm.Label>
              <Field type="number" name="qtd" component={AppFormControl} />
              <ErrorMessage name="qtd" component={BootForm.Text} />
            </BootForm.Group>

            <ButtonGroup>
              <Button variant="light" onClick={() => resetForm()}>
                {'Limpar'}
              </Button>

              <Button 
                type="submit" 
                variant="danger" 
                disabled={isSubmitting} 
                onClick={() => setFieldValue('op', 'REM')}
              >
                {'Remover'}
              </Button>

              <Button 
                type="submit" 
                disabled={isSubmitting} 
                onClick={() => setFieldValue('op', 'ADD')}
              >
                {'Adicionar'}
              </Button>
            </ButtonGroup>
          </Form>
        )}
      </Formik>
    );
  }

}

export default connect(produtoEstoqueFormSelector, { 
  addProdutoEstoque,
  listProduto,
  removeProdutoEstoque
})(
  EstoqueOperacaoForm
);
