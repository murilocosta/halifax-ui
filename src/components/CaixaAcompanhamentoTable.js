import React from 'react';
import { Table } from 'react-bootstrap';
import formatDateTime from '../services/formatDateTime';

const CaixaAcompanhamentoTable = ({ registros }) => (
  <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Emissão</th>
            <th>Descrição</th>
            <th>Valor</th>
          </tr>
        </thead>

        <tbody>
          {registros.map((registro, index) => (
            <tr key={index}>
              <td>{registro.id}</td>
              <td>{formatDateTime(registro.emissao)}</td>
              <td>{registro.descricao}</td>
              <td>{registro.valor}</td>
            </tr>
          ))}
        </tbody>
      </Table>
);

export default CaixaAcompanhamentoTable;
