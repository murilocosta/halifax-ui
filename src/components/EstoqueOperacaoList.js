import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { ListGroup } from 'react-bootstrap';
import property from 'lodash/property';

import { estoqueOperacaoListSelector } from '../store/selectors/estoqueSelector';
import { listEstoque, selectEstoqueOperacao } from '../store/actions/estoqueActions';
import { listProdutoEstoque } from '../store/actions/produtoEstoqueActions';

function EstoqueOperacao({ estoque, estoques, listProdutoEstoque, selectEstoqueOperacao }) {
  useEffect(function () {
    listEstoque();
  }, []);

  function _handleClick(estoqueId) {
    selectEstoqueOperacao(estoqueId);
    listProdutoEstoque(estoqueId);
  }

  return (
    <ListGroup variant="pills" className="flex-column" defaultActiveKey={property('id')(estoque)}>
      {estoques.map(e => (
        <ListGroup.Item action key={e.id} eventKey={e.id} onClick={() => _handleClick(e.id)}>
          {e.nome}
        </ListGroup.Item>
      ))}
    </ListGroup>
  );
}

export default connect(estoqueOperacaoListSelector, {
  listProdutoEstoque,
  selectEstoqueOperacao
})(
  EstoqueOperacao
);
