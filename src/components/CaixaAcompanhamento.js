import React from 'react';
import { Row, Col } from 'react-bootstrap';

import CaixaTurnoList from './CaixaTurnoList';
import CaixaRegistroManager from './CaixaRegistroManager';

const CaixaAcompanhamento = () => (
  <div>
    <h2 className="page-header">Acompanhamento de caixa</h2>
    <br />
    <Row>
      <Col sm={2}>
        <CaixaTurnoList />
      </Col>
      <Col>
        <CaixaRegistroManager />
      </Col>
    </Row>
  </div>
);

export default CaixaAcompanhamento;