import React from 'react';
import { Row, Col } from 'react-bootstrap';

import CaixaTurnoList from './CaixaTurnoList';
import CaixaTurnoTable from './CaixaTurnoTable';

function CaixaTurnoManager() {
  return (
    <div>
      <h2 className="page-header">Turnos de caixa</h2>
      <br />
      <Row>
        <Col sm={2}>
          <CaixaTurnoList />
        </Col>
        <Col>
          <CaixaTurnoTable />
        </Col>
      </Row>
    </div>
  );
}

export default CaixaTurnoManager;
