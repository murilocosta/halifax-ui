import React from 'react';
import { connect } from 'react-redux';
import { Table } from 'react-bootstrap';

import { turnoCaixaListSelector } from '../store/selectors/caixaTurnoSelector';
import { closeTurnoCaixa, listTurnoByCaixa } from '../store/actions/caixaTurnoActions';
import CaixaTurnoTableRow from './CaixaTurnoTableRow';
import CaixaTurnoForm from './CaixaTurnoForm';

function _handleCloseTurno(turno, closeTurnoCaixa, listTurnoByCaixa) {
  closeTurnoCaixa(turno.id).then(function () {
    listTurnoByCaixa(turno.caixaId);
  });
}

const CaixaTurnoTable = ({
  turnoAberto,
  turnos,
  closeTurnoCaixa,
  listTurnoByCaixa
}) => (
    <React.Fragment>
      {!turnoAberto && <CaixaTurnoForm />}

      <Table responsive>
        <thead>
          <tr>
            <th>#</th>
            <th>Abertura</th>
            <th>Fechamento</th>
            <th>Saldo Inicial</th>
            <th>Saldo Final</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          {turnos.map((t, index) => (
            <CaixaTurnoTableRow
              key={index}
              turno={t}
              onCloseTurno={(turno) => _handleCloseTurno(turno, closeTurnoCaixa, listTurnoByCaixa)}
            />
          ))}
        </tbody>
      </Table>
    </React.Fragment>
  );

export default connect(turnoCaixaListSelector, {
  closeTurnoCaixa,
  listTurnoByCaixa
})(
  CaixaTurnoTable
);
