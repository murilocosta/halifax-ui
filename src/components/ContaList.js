import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/es/Table';

import { contaListSelector } from '../store/selectors/contaSelector';
import { listConta, selectConta } from '../store/actions/contaActions';
import ContaListItem from './ContaListItem';

const tableActionsStyle = { textAlign: 'right' };

function ContaList({ contas, selectConta }) {
  useEffect(function () {
    listConta();
  }, []);

  return (
    <Table responsive>
      <thead>
        <tr>
          <th>#</th>
          <th>Nome</th>
          <th style={tableActionsStyle}>Ações</th>
        </tr>
      </thead>
      <tbody>
        {(contas || []).map((e) => (
          <ContaListItem key={e.id} conta={e} onSelectConta={selectConta} />
        ))}
      </tbody>
    </Table>
  );
}

export default connect(contaListSelector, { selectConta })(ContaList);
