import React from 'react';
import { Button, Modal } from 'react-bootstrap';

const AppModal = ({ title, show, onClose, children }) => (
  <Modal show={show} onHide={onClose}>
    <Modal.Header closeButton>
      <Modal.Title>{title}</Modal.Title>
    </Modal.Header>
    <Modal.Body>{children}</Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onClick={onClose}>Fechar</Button>
    </Modal.Footer>
  </Modal>
);

export default AppModal;