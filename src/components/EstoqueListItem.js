import React from 'react';
import { connect } from 'react-redux';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';
import Button from 'react-bootstrap/es/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';

import { selectEstoque } from '../store/actions/estoqueActions';

const tableActionsStyle = { textAlign: 'right' };

function EstoqueListItem({ estoque, selectEstoque }) {
  return (
    <tr>
      <td>{estoque.id}</td>
      <td>{estoque.nome}</td>
      <td style={tableActionsStyle}>
        <ButtonGroup>
          <Button title={'Atualizar'} onClick={() => selectEstoque(estoque.id)}>
            <FontAwesomeIcon icon={faPen} />
          </Button>
        </ButtonGroup>
      </td>
    </tr>
  );
}

export default connect(null, { selectEstoque })(EstoqueListItem);