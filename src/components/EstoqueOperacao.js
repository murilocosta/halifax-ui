import React from 'react';
import { connect } from 'react-redux';
import { Col, Row } from 'react-bootstrap';

import { estoqueOperacaoSelector } from '../store/selectors/estoqueSelector';
import EstoqueOperacaoList from './EstoqueOperacaoList';
import EstoqueOperacaoTable from './EstoqueOperacaoTable';
import EstoqueOperacaoForm from './EstoqueOperacaoForm';

const EstoqueOperacao = ({ estoque }) => (
  <div>
    <h2 className="page-header">Operações de estoque</h2>
    <br />
    <Row>
      <Col sm={2}>
        <EstoqueOperacaoList />
      </Col>
      <Col>
        {estoque && (
          <React.Fragment>
            <EstoqueOperacaoForm />
            <br />
            <EstoqueOperacaoTable />
          </React.Fragment>
        )}
      </Col>
    </Row>
  </div>
);

export default connect(estoqueOperacaoSelector, {})(EstoqueOperacao);
