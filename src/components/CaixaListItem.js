import React from 'react';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';
import Button from 'react-bootstrap/es/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';

const tableActionsStyle = { textAlign: 'right' };

export default function CaixaListItem({ caixa, onSelectCaixa }) {
  return (
    <tr>
      <td>{caixa.id}</td>
      <td>{caixa.nome}</td>
      <td style={tableActionsStyle}>
        <ButtonGroup>
          <Button title={'Atualizar'} onClick={() => onSelectCaixa(caixa.id)}>
            <FontAwesomeIcon icon={faPen} />
          </Button>
        </ButtonGroup>
      </td>
    </tr>
  );
}
