import React from 'react';
import { connect } from 'react-redux';
import ButtonGroup from 'react-bootstrap/es/ButtonGroup';
import Button from 'react-bootstrap/es/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';

import { selectProduto } from '../store/actions/produtoActions';

const tableActionsStyle = { textAlign: 'right' };

function ProdutoListItem({ produto, selectProduto }) {
  return (
    <tr>
      <td>{produto.id}</td>
      <td>{produto.cod}</td>
      <td>{produto.descricao}</td>
      <td style={tableActionsStyle}>
        <ButtonGroup>
          <Button title={'Atualizar'} onClick={() => selectProduto(produto.id)}>
            <FontAwesomeIcon icon={faPen} />
          </Button>
        </ButtonGroup>
      </td>
    </tr>
  );
}

export default connect(null, { selectProduto })(ProdutoListItem);