import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Alert, Table } from 'react-bootstrap';
import isEmpty from 'lodash/isEmpty';

import { caixaListSelector } from '../store/selectors/caixaSelector';
import { listCaixa, selectCaixa } from '../store/actions/caixaActions';
import AppLoading from './AppLoading';
import CaixaListItem from './CaixaListItem';

const tableActionsStyle = { textAlign: 'right' };

function CaixaList({ caixas, listCaixa, selectCaixa }) {
  const [loading, setLoading] = useState(false);

  useEffect(function () {
    setLoading(true);
    listCaixa().then(function () {
      setLoading(false);
    });
  }, [listCaixa]);

  if (loading) {
    return <AppLoading />;
  }

  if (isEmpty(caixas)) {
    return <Alert variant="info">{'Não há caixas cadastrados'}</Alert>;
  }

  return (
    <Table responsive>
      <thead>
        <tr>
          <th>#</th>
          <th>Nome</th>
          <th style={tableActionsStyle}>Ações</th>
        </tr>
      </thead>
      <tbody>
        {caixas.map((e) => (
          <CaixaListItem key={e.id} caixa={e} onSelectCaixa={selectCaixa} />
        ))}
      </tbody>
    </Table>
  );
}

export default connect(caixaListSelector, { listCaixa, selectCaixa })(CaixaList);
