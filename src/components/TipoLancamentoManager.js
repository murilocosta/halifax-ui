import React from 'react';

import TipoLancamentoForm from './TipoLancamentoForm';
import TipoLancamentoList from './TipoLancamentoList';

function TipoLancamentoManager() {
  return (
    <section>
      <h2 className="page-header">Gerenciar tipos de lancamento</h2>
      <TipoLancamentoForm />
      <br />
      <TipoLancamentoList />
    </section>
  );
}

export default TipoLancamentoManager;
