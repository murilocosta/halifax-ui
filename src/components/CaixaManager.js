import React from 'react';

import CaixaForm from './CaixaForm';
import CaixaList from './CaixaList';

export default function CaixaManager() {
  return (
    <section>
      <h2 className="page-header">Gerenciar caixas</h2>
      <CaixaForm />
      <br />
      <CaixaList />
    </section>
  );
}
