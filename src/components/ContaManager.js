import React from 'react';

import ContaForm from './ContaForm';
import ContaList from './ContaList';

function ContaManager() {
  return (
    <section>
      <h2 className="page-header">Gerenciar contas</h2>
      <ContaForm />
      <br />
      <ContaList />
    </section>
  );
}

export default ContaManager;
