import React from 'react';
import Nav from 'react-bootstrap/es/Nav';
import Navbar from 'react-bootstrap/es/Navbar';
import NavDropdown from 'react-bootstrap/es/NavDropdown';
import { Link, NavLink } from 'react-router-dom';

export default function AppHeader() {
  return (
    <Navbar>
      <Navbar.Brand>
        <Link to={'/'}>Halifax</Link>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <NavDropdown title="Cadastros">
            <NavDropdown.Item as={NavLink} to={'/estoques'} exact>Estoques</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={'/produtos'}>Produtos</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={'/caixas'} exact>Caixas</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={'/contas'}>Contas</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={'/tipos-lancamento'}>Tipos de lançamento</NavDropdown.Item>
          </NavDropdown>
          <Nav.Item>
            <Nav.Link as={NavLink} to={'/estoques/operacoes'}>Estoque</Nav.Link>
          </Nav.Item>
          <NavDropdown title="Caixa">
            <NavDropdown.Item as={NavLink} to={'/caixas/turnos'}>Turnos</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={'/caixas/acompanhamento'}>Acompanhamento</NavDropdown.Item>
          </NavDropdown>
          <NavDropdown title="Financeiro">
            <NavDropdown.Item as={NavLink} to={'/financeiro/operacoes'}>Operações</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={'/financeiro/agendamentos'}>Agendamento</NavDropdown.Item>
            <NavDropdown.Item as={NavLink} to={'/financeiro/contas'}>Acompanhamento</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
