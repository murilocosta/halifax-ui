import * as Yup from 'yup';

const ContaRegistroSchema = Yup.object().shape({
  caixaTurnoId: Yup.number()
    .required('Turno do caixa é obrigatório!'),
  descricao: Yup.string()
    .min(3, 'Deve ter no minimo 3 caracteres!')
    .max(80, 'Deve ter no máximo 120 caracteres!')
    .required('Descrição é obrigatória!'),
  valor: Yup.number()
    .moreThan(-1, 'Deve ser positivo!')
    .required('Valor é obrigatório!'),
  emissao: Yup.date()
    .required('Data de emissão é obrigatória!')
});

export default ContaRegistroSchema;
