import * as Yup from 'yup';

const ContaSchema = Yup.object().shape({
  caixaId: Yup.number()
    .required('Caixa é obrigatório!'),
  fundoCaixa: Yup.number()
    .moreThan(-1, 'Deve ser positivo!')
    .required('Fundo de caixa é obrigatório!')
});

export default ContaSchema;
