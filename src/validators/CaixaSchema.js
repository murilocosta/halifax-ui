import * as Yup from 'yup';

const CaixaSchema = Yup.object().shape({
  nome: Yup.string()
    .min(3, 'Deve ter no minimo 3 caracteres')
    .max(120, 'Deve ter no máximo 120 caracteres')
    .required('Nome é obrigatório!')
});

export default CaixaSchema;
