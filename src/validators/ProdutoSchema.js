import * as Yup from 'yup';

const EstoqueSchema = Yup.object().shape({
  cod: Yup.string()
    .min(3, 'Deve ter no minimo 3 caracteres')
    .max(32, 'Deve ter no máximo 32 caracteres')
    .required('Código é obrigatório!'),
  descricao: Yup.string()
    .min(3, 'Deve ter no minimo 3 caracteres')
    .max(120, 'Deve ter no máximo 120 caracteres')
    .required('Descrição é obrigatória!')  
});

export default EstoqueSchema;
