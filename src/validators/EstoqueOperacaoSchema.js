import * as Yup from 'yup';

const EstoqueOperacaoSchema = Yup.object().shape({
  estoqueId: Yup.number()
    .required('Estoque é obrigatório!'),
  produto: Yup.array()
    .required('Produto é obrigatório!'),
  qtd: Yup.number()
    .min(1, 'Quantidade deve ser no mínimo 1')
    .required('Quantidade é obrigatória!')
});

export default EstoqueOperacaoSchema;
