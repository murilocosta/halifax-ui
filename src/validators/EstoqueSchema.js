import * as Yup from 'yup';

const EstoqueSchema = Yup.object().shape({
  nome: Yup.string()
    .min(3, 'Deve ter no minimo 3 caracteres')
    .max(130, 'Deve ter no máximo 130 caracteres')
    .required('Nome é obrigatório!')
});

export default EstoqueSchema;
