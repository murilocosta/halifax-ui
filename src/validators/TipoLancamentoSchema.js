import * as Yup from 'yup';

const TipoLancamentoSchema = Yup.object().shape({
  nome: Yup.string()
    .min(3, 'Deve ter no minimo 3 caracteres')
    .max(80, 'Deve ter no máximo 80 caracteres')
    .required('Nome é obrigatório!')
});

export default TipoLancamentoSchema;
